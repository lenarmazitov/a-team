export default function(sequelize, DataTypes) {
  let Employee = sequelize.define('Employee', {
    firstName: {
      type: DataTypes.STRING,
      field: 'first_name',
    },
    lastName: {
      type: DataTypes.STRING,
      field: 'last_name',
    },
  }, {
    tableName: 'employees',
    underscored: true,
    timestamps: false,
    classMethods: {
      associate : function(models) {
        Employee.belongsTo(models.Department)
      },
    },
  })

  return Employee
}
