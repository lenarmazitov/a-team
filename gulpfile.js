'use strict';

var gulp = require('gulp');
var path = require('path');
var merge = require('merge-stream');

gulp.task('startdev', function (cb) {
  startNodemon('development', cb);
});

gulp.task('startprod', function (cb) {
  startNode('production', cb);
});

gulp.task('clean', function (cb) {
  var fse = require('fs.extra');
  fse.rmrfSync(path.join(__dirname, '/build/'));
  fse.rmrfSync(path.join(__dirname, '/compiled/'));
  cb();
});

gulp.task('compile', function (cb) {
  var babel = require('gulp-babel');
  var js = gulp.src(['src/**/*.{js,jsx}'])
    .pipe(babel())
    .pipe(gulp.dest('./compiled/'));

  var other = gulp.src(['src/**/*', '!src/**/*.{js,jsx}'])
    .pipe(gulp.dest('./compiled/'));

  return merge(js, other);
});

gulp.task('webpack', function (cb) {
  process.env.NODE_ENV = 'production';
  var webpack = require('webpack');
  var config = require('./webpack.config.js')
  // returns a Compiler instance
  var compiler = webpack(config);

  compiler.run(function(err, stats) {
    cb();
  });
});

gulp.task('webpack-dev-server', function (cb) {
  process.env.NODE_ENV = 'development';
  var webpack = require('webpack');

  var config = require('./webpack.config.js');
  config.entry.main.unshift('webpack-dev-server/client', 'webpack/hot/dev-server');

  var compiler = webpack(config);

  var WebpackDevServer = require('webpack-dev-server');
  var server = new WebpackDevServer(compiler, config.devServer);

  server.listen(8080, "localhost", function(err) {
      // keep the server alive or continue?
      // callback();
  });
});

gulp.task('devserver', gulp.parallel('webpack-dev-server', 'startdev', function (done) {
  done();
}));

// this is needed for @DEPLOY task (look at deploy.sh)
gulp.task('build', gulp.series('clean', 'compile', 'webpack'));

gulp.task('default', gulp.series('devserver'));

function startNode (env, cb) {
  var spawn = require('child_process').spawn;

  var startNodeProcess = spawn('node', ['compiled/server.js'], {
    cwd: __dirname,
    env: {
      NODE_ENV: env
    },
    stdio: 'inherit'
  });
}

function startNodemon (env, cb) {
  var execMap = {
    js: 'npm run babel-node',
    jsx: 'npm run babel-node'
  };

  var nodemon = require('gulp-nodemon');
  var nodemonProcess = nodemon({
    script: 'src/server.js',
    execMap: execMap,
    ext: 'js jsx',
    ignore: ['node_modules/', 'compiled/']
  });

  nodemonProcess.on('start', function () {
    console.log('nodemon started');
  });
  nodemonProcess.on('crash', function () {
    console.log('nodemon crashed');
  });
  nodemonProcess.on('exit', function () {
    console.log('nodemon cleanly exited');
  });
  nodemonProcess.on('restart', function () {
    console.log('nodemon child process has restarted');
  });
  nodemonProcess.on('stdout', function (data) {
    console.log(`childProcess: ${data}`);
  });
  nodemonProcess.on('stderr', function (data) {
    console.log(`childProcess: ${data}`);
  });
}
