import employee from './employee'
import department from './department'
import loading from './loading'

export default {
  employee,
  department,
  loading
}
