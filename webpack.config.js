'use strict';

var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

const PATHS = {
  public: {
    fonts: '/public/fonts/'
  }
};

var config = {
  target: 'web',

  context: path.join(__dirname, '/src'),

  entry: {
    main: ['./client.js'],
    vendor: ['babel-polyfill']
  },

  output: {
    path: path.join(__dirname, '/build/'),
    filename: '[name].js',
    chunkFilename: '[name].[id].js',
    publicPath: process.env.NODE_ENV === 'production' ? '' : '/'
  },

  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.jsx', '.json']
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: [/node_modules/],
        query: {
          babelrc: false,
          plugins: ['transform-runtime', 'transform-object-rest-spread', 'transform-decorators-legacy', 'transform-class-properties'],
          presets: [
            'es2015',
            'stage-0',
            'react',
            'react-hmre'
          ]
        }
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader'
        })
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file-loader',
        query: {
          name: '[path][name].[ext]?[hash]'
        }
      },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' },
      {
        test: /\.html$/,
        loader: 'file-loader',
        query: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.ttf$/,
        loader: 'file-loader',
        query: {
          name: '[hash].[ext]'
        },
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: 'styles.css',
      allChunks: true,
      disable: process.env.NODE_ENV === 'development'
    }),
    // new CopyWebpackPlugin([
    //   {
    //     from: process.env.NODE_ENV === 'production' ? 'public/templates/index-static.html' : 'public/templates/index.html',
    //     to: 'index.html'
    //   },
    // ]),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ],

  devtool: 'sourcemap',

  devServer: {
    host: 'localhost',
    port: 8080,
    contentBase: path.join(__dirname, '/build/'),
    hot: true,
    proxy: {
      '*': 'http://localhost:3000'
    },
    stats: {colors: true},
    noInfo: true
  }
};

module.exports = config;
