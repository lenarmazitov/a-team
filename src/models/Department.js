
export default function(sequelize, DataTypes) {
  let Department = sequelize.define('Department', {
    name: {
      type: DataTypes.STRING,
    }
  }, {
    tableName: 'departments',
    timestamps: false,
  })
  return Department
}
