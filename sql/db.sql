CREATE DATABASE test COLLATE utf8_general_ci;

USE test;

CREATE TABLE departments(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)ENGINE=InnoDB collate=utf8_general_ci;

CREATE TABLE employees(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  department_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (department_id)
    REFERENCES departments(id)
    ON DELETE CASCADE
)ENGINE=InnoDB collate=utf8_general_ci;

CREATE INDEX department_id ON employee(department_id);
