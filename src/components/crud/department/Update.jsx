import React from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, FormControl, Col, ControlLabel, Button } from 'react-bootstrap'

import { FETCH_DEPARTMENTS, UPDATE_DEPARTMENT } from '../../../constants/actions'

@connect((state, ownProps) => ({
  id: ownProps.params.id,
  name: state.department[ownProps.params.id] && state.department[ownProps.params.id].name
}))
class Update extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.id,
      name: props.name,
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  // тут нужно подумать, возможно не стоит использовать одновременно control forms и uncontrol
  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.name
    })
  }

  componentDidMount() {
    this.props.dispatch({
      type: FETCH_DEPARTMENTS,
      payload: { id: this.props.id }
    })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    this.props.dispatch({
      type: UPDATE_DEPARTMENT,
      payload: this.state
    })
    event.preventDefault()
  }

  render() {
    return (
      <Form horizontal onSubmit={this.handleSubmit} method="post">
        <FormGroup controlId="formHorizontalEmail">
          <Col componentClass={ControlLabel} sm={2}>
            Name
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="name"
              value={this.state.name}
              placeholder="Name"
              onChange={this.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit">
              Сохранить
            </Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }

}

export default Update
