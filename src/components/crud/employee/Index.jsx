import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { Table, Grid, Row, Col } from 'react-bootstrap'

import {
  FETCH_EMPLOYEES,
  DELETE_EMPLOYEE,
  LOADING_START,
} from '../../../constants/actions'

@connect((state) => ({
  loading: state.loading,
  items: Object.values(state.employee)
}))
class Index extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number,
      firstName: React.PropTypes.string,
      lastName: React.PropTypes.string,
      Department: React.PropTypes.shape({
        id: React.PropTypes.number,
        name: React.PropTypes.string,
      })
    })),
  }

  static defaultProps = {
    loading: false,
    items: [],
  }

  componentDidMount() {
    this.props.dispatch({
      type: LOADING_START,
    })
    this.props.dispatch({
      type: FETCH_EMPLOYEES,
    })
  }

  handleDelete(event, id) {
    if (global.confirm && global.confirm('Удалить работника?')) {
      this.props.dispatch({
        type: DELETE_EMPLOYEE,
        payload: { id }
      })
    }
    event.preventDefault()
  }

  render() {
    return (
      <div>
        {this.props.loading &&
          <div>Загружаем...</div>
        }
        {!this.props.loading &&
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Employee Name</th>
                <th>Department</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.props.items.map((item, key) => {
                return (
                  <tr key={key}>
                    <td>{item.id}</td>
                    <td>{item.firstName} {item.lastName}</td>
                    <td>{item.Department && item.Department.name}</td>
                    <td>
                      <Row>
                        <Col xs={12} className="text-right">
                          <Link to={`/employee/update/${item.id}`}>[Ред.]</Link>
                          <Link to="#" onClick={(event) => { this.handleDelete(event, item.id) }}>[Удл.]</Link>
                        </Col>
                      </Row>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        }
        <Row>
          <Col xs={12} className="text-right"><Link to="/employee/create">Создать</Link></Col>
        </Row>
      </div>
    )
  }

}

export default Index
