import { indexBy } from './utils'
import {
  FETCH_DEPARTMENTS_SUCCESS,
  SAVE_DEPARTMENT_SUCCESS,
  DELETE_DEPARTMENT_SUCCESS,
} from '../constants/actions'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_DEPARTMENT_SUCCESS:
      state[action.result.id] = action.result
      return {...state}
    case DELETE_DEPARTMENT_SUCCESS:
      delete state[action.result]
      return {...state}
    case FETCH_DEPARTMENTS_SUCCESS:
      return indexBy(action.result, 'id')
    default:
      return state
  }
}
