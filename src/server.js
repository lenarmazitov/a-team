import Koa from 'koa'
import Router from 'koa-router'
import views from 'koa-views'
import departmentApi from './api/department'
import employeeApi from './api/employee'

const app = new Koa()
const router = new Router()

app
  .use(views(__dirname + '/public/templates/', { extension: 'html' }))
  .use(router.routes())

router.use('/api/v1/department', departmentApi.routes())
router.use('/api/v1/employee', employeeApi.routes())

router.get('*', (ctx, next) => {
  return ctx.render('index')
})

const PORT = process.env.PORT || 3000

app.listen(PORT)
