import React from 'react'

import { Grid, Row, Col, Clearfix } from 'react-bootstrap'
import { Link } from 'react-router'

class App extends React.Component {
  static propTypes = {
    menuItems: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      route: React.PropTypes.string,
    }))
  }
  // меню по хорошему нужно вынести в отдельный компонент
  static defaultProps = {
    menuItems: [
      { label: 'Departments', 'route': '/department' },
      { label: 'Employees', 'route': '/employee' },
    ]
  }

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={4}>
            <Clearfix>
              <ul className="menu open">
              {this.props.menuItems.map((item, key) => {
                return (
                  <li key={key}><Link to={item.route}>{item.label}</Link></li>
                )
              })}
              </ul>
            </Clearfix>
          </Col>
          <Col xs={8}>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    )
  }

}

export default App
