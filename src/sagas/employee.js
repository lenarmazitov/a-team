import { call, put, takeLatest, takeEvery } from 'redux-saga/effects'
import FormData from 'form-data'
import { push } from 'react-router-redux'

import { prepareFormData } from './utils'

import {
  FETCH_EMPLOYEES,
  CREATE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  DELETE_EMPLOYEE,
  FETCH_EMPLOYEES_SUCCESS,
  SAVE_EMPLOYEE_SUCCESS,
  SAVE_EMPLOYEE_ERROR,
  DELETE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_ERROR,
  LOADING_COMPLETE,
} from '../constants/actions'

function* fetchEmployeesWorker(action) {
  if (action && action.payload) {
    const response = yield call(fetch, `/api/v1/employee/${action.payload.id}`)
    const result = yield response.json()
    yield [
      put({type: FETCH_EMPLOYEES_SUCCESS, result: result}),
      put({type: LOADING_COMPLETE}),
    ]
  } else {
    const response = yield call(fetch, '/api/v1/employee')
    const result = yield response.json()
    yield [
      put({type: FETCH_EMPLOYEES_SUCCESS, result: result}),
      put({type: LOADING_COMPLETE}),
    ]
  }
}

function* fetchEmployees() {
  yield takeLatest(FETCH_EMPLOYEES, fetchEmployeesWorker)
}

function* saveEmployeeWorker(action) {
  try {
    let response
    if (action.payload.id) {
      response = yield call(fetch, `/api/v1/employee`, {
        method: 'PUT',
        body: prepareFormData(action.payload),
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    } else {
      response = yield call(fetch, '/api/v1/employee', {
        method: 'POST',
        body: prepareFormData(action.payload),
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    }
    const result = yield response.json()

    yield [
      put({type: SAVE_EMPLOYEE_SUCCESS, result: result}),
      put(push('/employee'))
    ]
  } catch(e) {
    yield put({type: SAVE_EMPLOYEE_ERROR, result: e.message})
  }
}

function* deleteEmployeeWorker(action) {
  try {
    const response = yield call(fetch, `/api/v1/employee/${action.payload.id}`, {
      method: 'DELETE',
    })

    const result = yield response.json()

    yield put({type: DELETE_EMPLOYEE_SUCCESS, result: result})
  } catch(e) {
    yield put({type: DELETE_EMPLOYEE_ERROR, result: e.message})
  }
}

function* saveEmployee() {
  yield [
    takeLatest(CREATE_EMPLOYEE, saveEmployeeWorker),
    takeEvery(UPDATE_EMPLOYEE, saveEmployeeWorker),
  ]
}

function* deleteEmployee() {
  yield [
    takeEvery(DELETE_EMPLOYEE, deleteEmployeeWorker)
  ]
}

export { fetchEmployees, saveEmployee, deleteEmployee }
