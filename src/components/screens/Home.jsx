import React from 'react'

import { Grid, Row, Col } from 'react-bootstrap'

class Home extends React.Component {

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12}>Home page</Col>
        </Row>
      </Grid>
    )
  }

}

export default Home
