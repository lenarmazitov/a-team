import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { Router, Route, IndexRoute, IndexRedirect, browserHistory, hashHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux'

import reducers from './reducers'
import rootSaga from './sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  combineReducers({
    ...reducers,
    routing: routerReducer,
  }),
  compose(
    applyMiddleware(sagaMiddleware),
    applyMiddleware(routerMiddleware(browserHistory)),
    typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
  )
)

sagaMiddleware.run(rootSaga)

let history
if (process.env.NODE_ENV === 'production') {
  history = syncHistoryWithStore(hashHistory, store)
} else {
  history = syncHistoryWithStore(browserHistory, store)
}

import App from './components/App'
import ErrorPage from './components/screens/ErrorPage'
import Home from './components/screens/Home'

// не стал заморачиваться с выносом роутов в другую директорию (сделал просто компонентами)
import DepartmentIndex from './components/crud/department/Index'
import DepartmentCreate from './components/crud/department/Create'
import DepartmentUpdate from './components/crud/department/Update'
import EmployeeIndex from './components/crud/employee/Index'
import EmployeeCreate from './components/crud/employee/Create'
import EmployeeUpdate from './components/crud/employee/Update'

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />

        <Route path="department">
          <IndexRoute component={DepartmentIndex} />
          <Route path="create" component={DepartmentCreate} />
          <Route path="update/:id" component={DepartmentUpdate} />
        </Route>

        <Route path="employee">
          <IndexRoute component={EmployeeIndex} />
          <Route path="create" component={EmployeeCreate} />
          <Route path="update/:id" component={EmployeeUpdate} />
        </Route>

        <Route path="*" component={ErrorPage}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('app')
)
