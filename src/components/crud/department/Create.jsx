import React from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, FormControl, Col, ControlLabel, Button } from 'react-bootstrap'

import { CREATE_DEPARTMENT } from '../../../constants/actions'

@connect((state) => ({

}))
class Create extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    this.props.dispatch({
      type: CREATE_DEPARTMENT,
      payload: this.state
    })
    event.preventDefault()
  }

  render() {
    return (
      <Form horizontal onSubmit={this.handleSubmit} method="post">
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            Name
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="name"
              value={this.state.name}
              placeholder="Name"
              onChange={this.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit">
              Создать
            </Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }

}

export default Create
