import {
  LOADING_START,
  LOADING_COMPLETE,
} from '../constants/actions'

const initialState = false

export default (state = initialState, action) => {
  switch (action.type) {
    case LOADING_COMPLETE:
      return false
    case LOADING_START:
      return true
    default:
      return state
  }
}
