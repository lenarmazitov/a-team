import { call, put, takeLatest, takeEvery } from 'redux-saga/effects'
import FormData from 'form-data'
import { push } from 'react-router-redux'

import { prepareFormData } from './utils'

import {
  FETCH_DEPARTMENTS,
  CREATE_DEPARTMENT,
  UPDATE_DEPARTMENT,
  DELETE_DEPARTMENT,
  FETCH_DEPARTMENTS_SUCCESS,
  SAVE_DEPARTMENT_SUCCESS,
  SAVE_DEPARTMENT_ERROR,
  DELETE_DEPARTMENT_SUCCESS,
  DELETE_DEPARTMENT_ERROR,
  LOADING_COMPLETE,
} from '../constants/actions'

function* fetchDepartmentsWorker(action) {
  if (action && action.payload) {
    const response = yield call(fetch, `/api/v1/department/${action.payload.id}`)
    const result = yield response.json()
    yield [
      put({type: FETCH_DEPARTMENTS_SUCCESS, result: result}),
      put({type: LOADING_COMPLETE}),
    ]
  } else {
    const response = yield call(fetch, '/api/v1/department')
    const result = yield response.json()
    yield [
      put({type: FETCH_DEPARTMENTS_SUCCESS, result: result}),
      put({type: LOADING_COMPLETE}),
    ]
  }
}

function* fetchDepartments() {
  yield takeLatest(FETCH_DEPARTMENTS, fetchDepartmentsWorker)
}

function* saveDepartmentWorker(action) {
  try {
    let response
    if (action.payload.id) {
      response = yield call(fetch, `/api/v1/department`, {
        method: 'PUT',
        body: prepareFormData(action.payload),
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    } else {
      response = yield call(fetch, '/api/v1/department', {
        method: 'POST',
        body: prepareFormData(action.payload),
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    }
    const result = yield response.json()

    yield [
      put({type: SAVE_DEPARTMENT_SUCCESS, result: result}),
      put(push('/department'))
    ]
  } catch(e) {
    yield put({type: SAVE_DEPARTMENT_ERROR, result: e.message})
  }
}

function* deleteDepartmentWorker(action) {
  try {
    const response = yield call(fetch, `/api/v1/department/${action.payload.id}`, {
      method: 'DELETE',
    })

    const result = yield response.json()

    yield put({type: DELETE_DEPARTMENT_SUCCESS, result: result})
  } catch(e) {
    yield put({type: DELETE_DEPARTMENT_ERROR, result: e.message})
  }
}

function* saveDepartment() {
  yield [
    takeLatest(CREATE_DEPARTMENT, saveDepartmentWorker),
    takeEvery(UPDATE_DEPARTMENT, saveDepartmentWorker),
  ]
}

function* deleteDepartment() {
  yield [
    takeEvery(DELETE_DEPARTMENT, deleteDepartmentWorker)
  ]
}

export { fetchDepartments, saveDepartment, deleteDepartment }
