import React from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, FormControl, Col, ControlLabel, Button } from 'react-bootstrap'

import { FETCH_EMPLOYEES, UPDATE_EMPLOYEE } from '../../../constants/actions'

@connect((state, ownProps) => ({
  id: ownProps.params.id,
  firstName: state.employee[ownProps.params.id] && state.employee[ownProps.params.id].firstName,
  lastName: state.employee[ownProps.params.id] && state.employee[ownProps.params.id].lastName,
}))
class Update extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.id,
      firstName: props.firstName,
      lastName: props.lastName,
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  // тут нужно подумать, возможно не стоит использовать одновременно control forms и uncontrol
  componentWillReceiveProps(nextProps) {
    this.setState({
      firstName: nextProps.firstName,
      lastName: nextProps.lastName,
    })
  }

  componentDidMount() {
    this.props.dispatch({
      type: FETCH_EMPLOYEES,
      payload: { id: this.props.id }
    })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    this.props.dispatch({
      type: UPDATE_EMPLOYEE,
      payload: this.state
    })
    event.preventDefault()
  }

  render() {
    return (
      <Form horizontal onSubmit={this.handleSubmit} method="post">
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            First name
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="firstName"
              value={this.state.firstName}
              placeholder="First name"
              onChange={this.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            Last name
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="lastName"
              value={this.state.lastName}
              placeholder="Last name"
              onChange={this.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit">
              Сохранить
            </Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }

}

export default Update
