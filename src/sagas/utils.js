function prepareFormData(payload) {
  let form = new FormData()
  for (let key in payload) {
    form.append(key, payload[key])
  }
  let formData = []
  for (var [key, value] of form.entries()) {
    formData.push(encodeURIComponent(key) + '=' + encodeURIComponent(value))
  }
  return formData.join('&')
}

export { prepareFormData }
