function indexBy(collection, key) {
  return collection.reduce((prev, cur) => {
    prev[cur[key]] = cur
    return prev
  }, {})
}

export { indexBy }
