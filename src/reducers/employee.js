import { indexBy } from './utils'
import {
  FETCH_EMPLOYEES_SUCCESS,
  SAVE_EMPLOYEE_SUCCESS,
  DELETE_EMPLOYEE_SUCCESS,
} from '../constants/actions'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_EMPLOYEE_SUCCESS:
      state[action.result.id] = action.result
      return {...state}
    case DELETE_EMPLOYEE_SUCCESS:
      delete state[action.result]
      return {...state}
    case FETCH_EMPLOYEES_SUCCESS:
      return indexBy(action.result, 'id')
    default:
      return state
  }
}
