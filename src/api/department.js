import Router from 'koa-router'
import BodyParser from 'koa-body'
import co from 'co'

import { Department } from '../models/'

const router = new Router()
const bodyParser = new BodyParser()

router.get('/', co.wrap(function* (ctx, next) {
  yield Department.findAll().then((data) => {
    ctx.body = JSON.stringify(data)
  })
}))

router.get('/:id', co.wrap(function* (ctx, next) {
  yield Department.findAll({where: {id: ctx.params.id}}).then((data) => {
    ctx.body = JSON.stringify(data)
  })
}))

router.post('/', bodyParser, co.wrap(function* (ctx, next) {
  yield Department.create(ctx.request.body).then((data) => {
    ctx.body = JSON.stringify(data)
  })
}))

router.put('/', bodyParser, co.wrap(function* (ctx, next) {
  let newData = Object.assign({}, ctx.request.body)
  newData.id = parseInt(newData.id)
  yield Department.update(newData, {where: {id: ctx.request.body.id}}).then((data) => {
    if (data) {
      ctx.body = JSON.stringify(newData)
    }
  })
}))

router.delete('/:id', co.wrap(function* (ctx, next) {
  yield Department.destroy({where: {id: ctx.params.id}}).then((data) => {
    if (data) {
      ctx.body = JSON.stringify(ctx.params.id)
    } else {
      ctx.body = JSON.stringify(0)
    }
  })
}))

export default router
