import React from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, FormControl, Col, ControlLabel, Button } from 'react-bootstrap'

import { CREATE_EMPLOYEE, LOADING_START, FETCH_DEPARTMENTS } from '../../../constants/actions'

@connect((state) => ({
  departments: Object.values(state.department)
}))
class Create extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool,
    departments: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number,
      name: React.PropTypes.string,
    }))
  }

  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      department_id: '',
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.props.dispatch({
      type: LOADING_START,
    })
    this.props.dispatch({
      type: FETCH_DEPARTMENTS,
    })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    this.props.dispatch({
      type: CREATE_EMPLOYEE,
      payload: this.state
    })
    event.preventDefault()
  }

  render() {
    return (
      <div>
        {this.props.loading &&
          <div>Загружаем...</div>
        }
        {!this.props.loading &&
          <Form horizontal onSubmit={this.handleSubmit} method="post">
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                First name
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  name="firstName"
                  value={this.state.firstName}
                  placeholder="First name"
                  onChange={this.handleInputChange}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                Last name
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  name="lastName"
                  value={this.state.lastName}
                  placeholder="Last name"
                  onChange={this.handleInputChange}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                Department
              </Col>
              <Col sm={10}>
                <FormControl
                  name="department_id"
                  onChange={this.handleInputChange}
                  componentClass="select"
                  placeholder="Department">
                  {this.props.departments.map((item, key) => {
                    return <option key={key} value={item.id}>{item.name}</option>
                  })}
                </FormControl>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col smOffset={2} sm={10}>
                <Button type="submit">
                  Создать
                </Button>
              </Col>
            </FormGroup>
          </Form>
        }
      </div>
    )
  }

}

export default Create
