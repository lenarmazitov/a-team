import { fork } from 'redux-saga/effects';
import {
  fetchDepartments,
  saveDepartment,
  deleteDepartment,
} from './department';

import {
  fetchEmployees,
  saveEmployee,
  deleteEmployee,
} from './employee';


export default function* rootSaga() {
    yield [
        fork(fetchDepartments),
        fork(saveDepartment),
        fork(deleteDepartment),
        fork(fetchEmployees),
        fork(saveEmployee),
        fork(deleteEmployee),
    ];
}
