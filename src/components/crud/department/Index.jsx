import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { Table, Grid, Row, Col } from 'react-bootstrap'

import {
  FETCH_DEPARTMENTS,
  DELETE_DEPARTMENT,
  LOADING_START,
} from '../../../constants/actions'

@connect((state) => ({
  loading: state.loading,
  items: Object.values(state.department)
}))
class Index extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool,
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number,
      name: React.PropTypes.string,
    })),
  }

  static defaultProps = {
    loading: false,
    items: [],
  }

  componentDidMount() {
    this.props.dispatch({
      type: LOADING_START,
    })
    this.props.dispatch({
      type: FETCH_DEPARTMENTS,
    })
  }

  handleDelete(event, id) {
    if (global.confirm && global.confirm('Удалить департамент?')) {
      this.props.dispatch({
        type: DELETE_DEPARTMENT,
        payload: { id }
      })
    }
    event.preventDefault()
  }

  render() {
    return (
      <div>
        {this.props.loading &&
          <div>Загружаем...</div>
        }
        {!this.props.loading &&
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Department Name</th>
              </tr>
            </thead>
            <tbody>
              {this.props.items.map((item, key) => {
                return (
                  <tr key={key}>
                    <td>{item.id}</td>
                    <td>
                      <Row><Col xs={8}>{item.name}</Col>
                        <Col xs={4} className="text-right">
                          <Link to={`/department/update/${item.id}`}>[Ред.]</Link>
                          <Link to="#" onClick={(event) => { this.handleDelete(event, item.id) }}>[Удл.]</Link>
                        </Col>
                      </Row>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        }
        <Row>
          <Col xs={12} className="text-right"><Link to="/department/create">Создать</Link></Col>
        </Row>
      </div>
    )
  }

}

export default Index
